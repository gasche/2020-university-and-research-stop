Parts of French academia are protesting against a "research funding
bill" whose structural changes would increase precarity in
universities and research institutions (for example by introducing the
dreaded "tenure track" model!).

A collective protest is planned starting March 5th, with the slogan:
"University and Research Stop".

This repository contains various documents related to those protests:
announce emails, presentations about the law, etc.

![Poster](pictures/5-mars.jpg)
